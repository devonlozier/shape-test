
// Circle.h - You CAN edit this file!

#pragma once

#include "Shape.h"

class Circle : public Shape
{
private:

	double m_radius = 1;

public:

	Circle() { }
	Circle(double radius) { SetRadius(radius); }
	virtual ~Circle() { }

	//Mutator (set)
	virtual void SetRadius(double radius);

	// define additional methods here:

	//Accessor(get)
	virtual double GetRadius() { return m_radius; }

	//virtual

	virtual std::string GetType() { return "circle"; }

	virtual double GetArea();

	virtual double GetPerimeter();

};